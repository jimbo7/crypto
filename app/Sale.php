<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'user_id', 'agent_id', 'client_id',
        'bank_id', 'description', 'amount', 'iso',
        'file', 'operation_method', 'agency_num',
        'price_btc', 'total_btc', 'active', 'user_ip', 'net_btc',
        'gross_btc', 'profits_btc'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id');
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    protected $casts = [
        'created_at' => 'datetime:H:i:s d/m/Y', // Change format
        'updated_at' => 'datetime:H:i:s d/m/Y',
	  ];

}
