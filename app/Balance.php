<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = [
        'web', 'amount', 'type', 'amount_operation',
        'amount_before_operation', 'active', 'notes',
        'amount_minus_operation','amount_sum_operation',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i:s d/m/Y',
        'updated_at' => 'datetime:H:i:s d/m/Y',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeInactive($query)
    {
        return $query->where('active', 0);
    }
}
