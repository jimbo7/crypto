<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $fillable = [
        'agent_id', 'name', 'notes', 'dni',
        'active', 'approved', 'email','phone',
        'id_f','id_b','birthdate', 'type_id',
        'term_selfie','accumulated','auth_image', 'country_id',
        'nation_id', 'gender', 'phone_code', 'last_name', 'address',
        'terms_accepted'
    ];

    protected $cast = ['sms_verified_at', 'email_verified_at', 'expiration_date_dni'];

    protected $hidden = ['sms_confirm_token', 'email_confirm_token' ];

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function native()
    {
        return $this->belongsTo(Country::class, 'nation_id', 'id');
    }

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }

    public function currency()
    {
        return $this->belongsToMany(Currency::class,'client_currency')->withPrivot('cummulated');
    }

}
