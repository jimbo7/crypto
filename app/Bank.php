<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'account_code', 'account_holder','description', 'active', 'email', 'dni_holder', 'phone_holder', 'address', 'office_num', 'bci'
    ];

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }

    public function agent()
    {
        return $this->hasMany(Agent::class);
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
