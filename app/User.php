<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = [ 'name', 'email', 'password',
       'role', 'active', 'agent_id',
       'last_login_at', 'last_login_ip',
      ];

    protected $hidden = ['password', 'token', 'remember_token'];

    // public function agent()
    // {
    //     return $this->belongsTo(Agent::class, 'agent_id', 'id');
    // }

    public function agent()
    {
        return $this->hasOne(Agent::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function isAdmin()
    {
      if ( $this->role == 3 ) {
          return true;
      }
      return false;
    }

    public function isAgent()
    {
      if ( $this->role == 1 ) {
          return true;
      }
      return false;
    }

}
