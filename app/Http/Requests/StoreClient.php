<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Http\FormRequest;

class StoreClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agent_id' => 'required',
            'name' => 'required',
            'last_name' => 'required',
            'dni' => 'required|unique:clients,dni',
            'email' => 'required|email|unique:clients,email',
            'country_id' => 'required',
            'type_id' => 'required',
            'id_b' => 'required',
            'id_f' => 'required',
            'expiration_date_dni' => 'required',
            'terms_accepted' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'id_b' => 'Subir id Reverso',
            'id_f' => 'Subir id Frontal',
            'type_id' => 'Tipo de Identificacion',
            'country_id' => 'Nacionalidad',
            'nation_id' => 'Pais donde Naciste',
            'expiration_date_dni' => 'Fecha de Vencimiento de Documento de Indentidad',
            'terms_accepted' => 'Aceptar términos y Condiciones'
        ];
    }
}
