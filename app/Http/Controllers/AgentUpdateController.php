<?php

namespace App\Http\Controllers;

use App\AgentUpdate;
use Illuminate\Http\Request;

class AgentUpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = AgentUpdate::orderBy('id', 'desc')->get();
        return response()->json(['agents' => $agents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AgentUpdate  $agentUpdate
     * @return \Illuminate\Http\Response
     */
    public function show(AgentUpdate $agentUpdate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AgentUpdate  $agentUpdate
     * @return \Illuminate\Http\Response
     */
    public function edit(AgentUpdate $agentUpdate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AgentUpdate  $agentUpdate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AgentUpdate $agentUpdate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AgentUpdate  $agentUpdate
     * @return \Illuminate\Http\Response
     */
    public function destroy(AgentUpdate $agentUpdate)
    {
        //
    }
}
