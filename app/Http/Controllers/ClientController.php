<?php
namespace App\Http\Controllers;

use DB;
use Auth;
use Image;
use Session;
use Redirect;
use App\Client;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Traits\ClientsTrait;
use Resources\Client as ClientResource;
use Resources\ClientCollection;
use Resources\ClientPaymentCollection;
use App\Http\Requests\StoreClient;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ClientRepositoryInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class ClientController extends Controller
{
    use ClientsTrait;

    public function __construct(ClientRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function sms()
    {
      return $this->repository->smsCheck();
    }

    public function index()
    {
        $clients = new ClientCollection(Client::with(['agent:id,name,username','native:id,name','country:id,name'])->get()->reverse());
        return response()->json(['clients' => $clients]);
    }

    public function indexCount()
    {
        $countClients = Client::count();

        $clients = Client::select('id','name')->orderBy('id', 'desc')->limit(3);

        return response()->json(['clients' => $clients, 'countClients' => $countClients]);
    }

    public function search(Request $request)
    {
        // $clients = Client::where('approved', 2)
        //     ->where(function($query) use ($request) {
        //         $query->where('dni', $request->dni)->orWhere('birthdate',$request->birthdate)->orWhere('name',$request->name)->orwhere('phone', $request->phone);
        //     })->get();

        $clients = Client::where('dni', $request->dni)->orWhere('birthdate',$request->birthdate)->orWhere('name', 'like', '%'. $request->name . '%')->orwhere('phone', $request->phone)->get();

        return response()->json(['clients'=>$clients]);
    }

    public function searchAdmin($name)
    {

        $clients = Client::where('name', 'like', '%'. $name . '%')->get();

        return response()->json(['clients'=>$clients]);
    }

    public function clientsApproveds()
    {
        $clients = new ClientPaymentCollection(Client::where('approved', 2)->get());
        return response()->json(['clients' => $clients]);
    }

    public function agentClients()
    {

        $clients = new ClientCollection(Client::where('agent_id',$id)->where('approved', 1)->get());

        return response()->json(['clients' => $clients]);
    }

    public function store(Request $request)
    {   
        
      $response = $this->clientStore($request);

      if ($response['client']) {
          return response()->json(['message'=>'Cliente Guardado, Estara en revision hasta ser Aprovado'], 200);
      } elseif ($response['error']) {
          return response()->json(['error'=>$response['error']], 400);
      }

    }

    public function storeFromWeb(StoreClient $request)
    {
        $phone = $request->phone_code.ltrim($request->phone, '0');
        
        $phone_true = Client::where('phone', $phone)->first();

        if ($phone_true)
        {
            return Redirect::back()->withErrors(['phone' => 'Numero telefonico ya registrado.']);
        }
        

        $response = $this->clientStore($request);
        
        if ($response['client']) {
            $client = $response['client'];

            $client->sms_confirm_token = rand(1024, 9962);

            $resp_sms = $this->repository->smsCheckCode($client);
            if ($resp_sms['http_status'] == 201) {
              $client->save();
              Session::flash('success', "Registrado!");
              return Redirect::route('clients-verify-sms', $client->id);
            } else {
              return redirect()->back()->with(['error' => 'Datos Guardados pero mensaje no enviado, algo mal con su numero contacte con '. $client->agent->name]);
            }

        } elseif ($response['error']) {
            return Redirect::back()->withErrors($resp.onse);
        }

    }

    public function clientCheckSms(Request $request, Client $client)
    {
        // dd($client);

        if ($client->sms_confirm_token && !$client->sms_verified_at) {
          if ($client->sms_confirm_token == $request->code) {
            $client->sms_confirm_token = '';
            $client->sms_verified_at = Carbon::now();
            $client->save();

            Session::flash('success', "Telefono Verificado Muchas gracias, Con esto hemos Finalizado!");
            return redirect()->back()->with(['message' => 'cliente comprobado']);

          } else {
            return Redirect::back()->withErrors(['error', 'Verifique el codigo']);
          }
        } elseif ($client->sms_verified_at && !$client->sms_confirm_token){
          Session::flash('error', "Ya Verificado!");
            return redirect()->back();
        } else {
          return Redirect::back()->withErrors(['error', 'usuario Verificado']);
        }
    }

    public function show($id)
    {

        $client = Client::find($id);
        return response()->json(new ClientResource($client));
    }

    public function edit(Client $client)
    {
        $client = Client::find($client);
        return response()->json($client);
    }

    public function update(Request $request,$id)
    {
        $img = 0;
        try {

            DB::beginTransaction();
            $client = Client::findOrFail($id);

            $client->update([
                'agent_id' => $request->agent_id,
                'name' => strtoupper($this->deleteAccent($request->name)),
                'last_name' => strtoupper($this->deleteAccent($request->last_name)),
                'type_id' => $request->type_id,
                'dni' => $request->dni,
                'phone' => $request->phone,
                'notes' => $request->notes,
                'email' => $request->email,
                'birthdate' => $request->birthdate,
                'gender' => $request->gender,
                'nation_id' => $request->nation_id,
                'country_id' => $request->country_id,
                'term_selfie' => $request->term_selfie,
                'address' => $request->address,
                'terms_accepted' => $request->terms_accepted,
            ]);

            if ($request->hasFile('id_f')) {
                $image = $request->file()['id_f'];
                $client->id_f = $this->saveImage($image,'clients/'.$client->id,$client->id);
                $img++;
            }

            if ($request->hasFile('id_b')) {
                $image = $request->file()['id_b'];
                $client->id_b = $this->saveImage($image,'clients/'.$client->id,$client->id);
                $img++;

            }

            if ($request->hasFile('term_selfie')) {
                $image = $request->file()['term_selfie'];
                $client->term_selfie = $this->saveImage($image,'clients/'.$client->id,$client->id);
                $img++;
            }

            if ($request->hasFile('auth_image')) {
                $image = $request->file()['auth_image'];
                $client->auth_image = $this->saveImage($image,'clients/'.$client->id,$client->id);
                $img++;
            }
            
            if ($img == 3 || $img ==4) {
                $client->approved=1;
            }
            $client->save();

            DB::commit();

            return response()->json(['message'=>'Cliente Actualizado, Estara en revision hasta ser Aprovado']);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error'=>$e], 400);
        }
    }

    public function desactive(Request $request)
    {
        $client = Client::find($request->id);
        $client->active = 0;
        $client->save();

        return response()->json('desactivado');
    }

    public function activate($id)
    {
        $client = Client::find($id);
        $client->active = 1;
        $client->save();

        return response()->json(['message' => 'Cliente activado']);
    }

    public function approve($id)
    {
        if (Auth::user()->role == 3) {
            
            $client = Client::find($id);
            if ($client->approved == 0 || $client->approved == 1) {
                $client->approved = 2;
                $client->save();

                return response()->json(['message' => 'Cliente Aprobado']);
            } else {
                $client->approved = 1;
                $client->save();

                return response()->json(['message' => 'Cliente Desaprobado']);
            }
        
        } else {
            return respose()->json(['error'=>'Unauthorized-jwt'], 401);
        }
    }

    public function destroy($id)
    {
        $client = Client::findOrFail($id);
        if ($client->id_b || $client->id_f) {
            $delete[] =Storage::delete([$client->id_b, $client->id_f]);
            // $delete = Storage::disk('public')->delete('/clients/'.$id);
            $delete[] = rmdir(storage_path('app/public/clients/'.$id));
            
        }
        $client->delete();

        return response()->json(['message' => 'Eliminado']);
    }

    public function emailV($email)
    {
        $e = Client::where('email', $email)->first();
        return response()->json(['email'=>$e]);
    }

    public function dniV($dni)
    {
        $d = Client::where('dni', $dni)->first();
        return response()->json(['dni'=>$d]);
    }

    public function phoneV($phone)
    {
        $p = Client::where('phone', $phone)->first();
        return response()->json(['phone'=>$p]);
    }

    public function totalCalculate()
    {
        $month = Carbon::now()->month;
        $year = Carbon::now()->year;

        $clients = Client::all();
        $totalSale = [];
        $total = 0;

        for ($c=0; $c <count($clients); $c++) {

          $amount = Sale::where('client_id', $clients[$c]->id)
                        ->whereYear('created_at', '=', $year)
                        ->whereMonth('created_at', '=', $month)
                        ->where('iso', 'EUR')->sum('amount');

          if ($amount != '') {
              Client::where('id', $clients[$c]->id)->update([ 'accumulated' => $amount ]);
              $total = $total + $amount;
              $arr = array_push($totalSale, ['client_id' => $clients[$c]->id, 'amount' => $amount]);
          }

        }

        return response()->json(['totalrray'=>$totalSale, 'amount' => $total ], 200);
    }

    public function resetMounthly()
    {
        $clients = Client::all();

        for ($c=0; $c <count($clients) ; $c++) {
            $client = Client::where('id', $clients[$c]->id)->update(['accumulated' => 0]);
        }
        return response()->json(['message' => 'Ok! month reset']);
    }

    public function saveImage($image, $path, $id)
    {
        $date = Carbon::now();
        $extension = $image->extension();
        $name = $id. '-'. $date->format('d-m-Y-H-m-s');
        $url = $image->store($path);

        return $url;
    }

}
