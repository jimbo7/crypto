<?php

namespace App\Http\Controllers;

use Auth;
use App\Price;
use App\Percent;
use App\Currency;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',  ['except' => ['getPrice', 'ratePage', 'setPrice']]);
    }

    public function index()
    {
        $pct = Percent::where('active',1)->with('currency:id,iso')->get();
        $prices = Price::with('currency:id,iso')->get();

        for ($i=0; $i < count($prices) ; $i++) {
            for ($j=0; $j < count($pct); $j++) {
                if ($prices[$i]->currency_id == $pct[$j]->currency_id) {
                    $prices[$i]->calculated = ($prices[$i]->amount*$pct[$j]->amount/100)+$prices[$i]->amount;
                    $prices[$i]->calc_format = number_format($prices[$i]->calculated,2,',','.');
                    $prices[$i]->amount_format = number_format($prices[$i]->amount,2,',','.');
                }
            }
        }
        return response()->json(['prices' => $prices->toArray()]);
    }

    public function getEur()
    {
        $price = Price::where('notes', 'eur')->first();
        $price->amount_formated = number_format($price->amount,2,'.',',');
        return response()->json(['price' => $price]);
    }

    public function last()
    {
        $price = Price::orderBy('id', 'desc')->first();
        return response()->json(['price' => $price]);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'amount' => 'required|numeric',
            'currency_id' => 'required'
        ]);
        if($validator->fails()){
            return response()->json( $validator->messages(), 403);
        }

        $price = new Price ([
            'amount' => $request->amount,
            'notes' => $request->notes,
            'currency_id' => $request->currency_id,
        ]);

        $price->save();

        return response()->json(['message' => 'Nuevo precio Guardado'], 200);
    }

    public function show($id)
    {
        $price = Price::findOrFail($id);
        return response()->json(['price' => $price]);
    }

    public function update(Request $request,$id )
    {
        $validator = Validator::make($request->all(),[
            'amount' => 'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['error' => $validator->messages()], 403);
        }

        Price::where('id',$request->id)->update([
            'amount' => $request->amount,
            'notes' => $request->notes,
            'currency_id' => $request->currency_id,
            'updated_at' => Carbon::now(),
        ]);


        return response()->json(['message' => 'Modificado con Exito'], 200);
    }

    public function getPrice()
    {
        $pct = Percent::where('active',1)->wherenotNull('currency_id')->with('currency:id,iso')->get();
        $prices = Price::with('currency:id,iso,symbol')->get();
        $pr = [];
        $discount = 0;
        $user = Auth::user();

        if ($user && $user->agent) {
            if ($user->agent->discount) {
                $discount = $user->agent->discount;
            }
        }
        for ($i=0; $i < count($prices) ; $i++) {
            for ($j=0; $j < count($pct); $j++) {
                if ($prices[$i]->currency_id == $pct[$j]->currency_id) {
                    $prices[$i]->calculated = ($prices[$i]->amount*$pct[$j]->amount/100)+$prices[$i]->amount;
                    $prices[$i]->calculated = $prices[$i]->calculated-($prices[$i]->calculated*$discount);
                    $pr[$i]['gross'] = $prices[$i]->amount;
                    $pr[$i]['amount'] = $prices[$i]->calculated;
                    $pr[$i]['id'] = $prices[$i]->id;
                    $pr[$i]['currency_id'] = $prices[$i]->currency_id;
                    $pr[$i]['calculated'] = $prices[$i]->calculated;
                    $pr[$i]['calc_format'] = number_format($prices[$i]->calculated,2,',','.');
                    $pr[$i]['iso'] = $prices[$i]->currency->iso;
                    $pr[$i]['symbol'] = $prices[$i]->currency->symbol;
                }
            }
        }

        return response()->json(['prices' =>$pr]);
    }

    public function ratePage()
    {
        $pct = Percent::where('active',1)->wherenotNull('currency_id')->with('currency:id,iso,symbol')->get();
        $prices = Price::with('currency:id,iso,symbol')->get();

        for ($i=0; $i < count($prices) ; $i++) {
            for ($j=0; $j < count($pct); $j++) {
                if ($prices[$i]->currency_id == $pct[$j]->currency_id) {
                    $prices[$i]->calculated = ($prices[$i]->amount*$pct[$j]->amount/100)+$prices[$i]->amount;
                    $pr[$i]['iso'] = $prices[$i]->currency->iso;
                    $pr[$i]['symbol'] = $prices[$i]->currency->symbol;
                    $pr[$i]['amount'] = number_format($prices[$i]->calculated,2,',','.');
                }
            }
        }

        return view('pages.rate', compact('pr'));
    }


    public function setPrice()
    {
        $pct = Percent::where('active',1)->first();
        $url = 'https://localbitcoins.com/api/equation/btc_in_usd*USD_in_';
        $currencies = Currency::active()->get();

        for ($i=0; $i <count($currencies) ; $i++) {

            $localBitcoin = Http::get($url.$currencies[$i]->iso)->throw()->json();

            $amount = ($localBitcoin['data']*$pct->amount/100)+$localBitcoin['data'];

            $prices[$i] = [
                'iso' => $currencies[$i]->iso,
                'amount_calc' => number_format($amount, 2),
                'amount_lb' => number_format($localBitcoin['data'],2)
            ];

            $price = Price::where('currency_id', $currencies[$i]->id)->orWhere('notes', $currencies[$i]->iso)->first();
            $price->amount = $localBitcoin['data'];
            $price->updated_at = Carbon::now();
            $price->save();
        }

        return response()->json(['prices' =>$prices, 'pct' => $pct]);
    }

    public function reset()
    {
        Price::query()->delete();
        $currencies = Currencies::active()->get();
        for ($i=0; $i <count($currencies) ; $i++) {
            $price = new Price ([
                'amount' => 100,
                'notes' => $currencies[$i]->iso,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
            $price->save();
        }
        return redirect('/prices');
    }

    public function exchange()
    {
        return view('uni-price');
    }
}
