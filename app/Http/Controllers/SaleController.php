<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use App\Sale;
use App\Client;
use App\Balance;
use App\Http\Resources\SaleCollection;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Sale as SaleResource;
use App\Http\Requests\StoreSale;
use App\Http\Traits\HelpersTrait;
use Illuminate\Support\Facades\Auth;

class SaleController extends Controller
{
  use HelpersTrait;

  public function __construct()
  {
    $this->middleware('admin', ['except' => ['bankPdf', 'imgDownload']]);
  }

  public function index(Request $request)
  {
    if ($request->order == '0' || $request->order > 0) {
      $order = $request->order;
    } elseif (is_null($request->order)) {
      $order = 5;
    }

    $date = Carbon::today()->subDays(30);
    $sales = new SaleCollection(Sale::where('created_at', '>=', $date)
      ->with(['agent:id,username,name,wallet_num,updated_at', 'client:id,name,last_name,accumulated', 'bank:id,account_holder,name'])->orderBy('created_at', 'desc')
      ->where(function ($query) use ($order) {
        if ($order >= 0 && $order < 5) {
          return $query->where('active', $order);
        }
        if ($order == 5) {
          return $query->whereNotNull('active');
        }
      })->get());
    // $sales = Sale::where('created_at', '>=', $date)->with(['agent:id,username', 'client:id,name', 'bank:id,account_holder,name'])
    // ->orderBy('id', 'desc')->get();

    return response()->json(['sales' => $sales]);
  }

  public function indexCount()
  {
    $total = [];
    $amount = 0;
    $date = Carbon::now();
    $day = $date->day;
    $month = $date->month;
    $sales = Sale::whereMonth('created_at', '>=', $month)->get();

    $count = $sales->count();
    for ($d = 1; $d <= $day; $d++) {
      for ($i = 0; $i < $count; $i++) {
        if (Carbon::parse($sales[$i]->payment_date)->day == $d) {
          $amount += $sales[$i]->amount;
        }
      }
      $days[$d] = $d;
      $total[$d] = $amount;
      $amount = 0;
    }
    $count = $sales->count();
    return response()->json(['count' => $count, 'total' => $total, 'days' => $days]);
  }

  public function agentCount($id)
  {
    $total = [];
    $amount = 0;
    $date = Carbon::now();
    $day = $date->day;
    $month = $date->month;
    $sales = Sale::where('agent_id', $id)->whereMonth('created_at', '>=', $month)->get();

    $count = $sales->count();
    for ($d = 1; $d <= $day; $d++) {
      for ($i = 0; $i < $count; $i++) {
        if (Carbon::parse($sales[$i]->payment_date)->day == $d) {
          $amount += $sales[$i]->amount;
        }
      }
      $days[$d] = $d;
      $total[$d] = $amount;
      $amount = 0;
    }
    $count = $sales->count();
    return response()->json(['count' => $count, 'total' => $total, 'days' => $days]);
  }

  public function indexHistory(Request $request)
  {
    $first = empty($request->first) ? 0 : (int)request('first');
    $last = empty($request->last) ? 3000 : (int)request('last');
    $limits = [];
    if ($first == 0) {
      $limits = $this->pagesLimits($last);
    }

    $sales = Sale::whereBetween('id', [$first, $last])
      ->with([
        'agent:id,username', 'client:id,name',
        'bank:id,account_holder,name'
      ])
      ->orderBy('id', 'desc')->get();

    return response()->json(['sales' => $sales, 'limits' => $limits]);
  }

  public function store(StoreSale $request)
  {
    try {
      DB::beginTransaction();

      $validated = $request->validated();
      $sale = Sale::create([
        'agent_id' => $request->agent_id,
        'bank_id' => $request->bank_id,
        'client_id' => $request->client_id,
        'description' => $request->description,
        'price_btc' => $request->price_btc,
        'total_btc' => $request->total_btc,
        'iso' => $request->iso,
        'operation_method' => $request->operation_method,
        'amount' => $request->amount,
        'agency_num' => $request->agency_num,
        'code' => $request->code,
        'active' => $request->active,
        'user_id' => Auth::user()->id,
        'user_ip' => $this->getIp(),
      ]);

      if ($request->hasFile('file')) {

        if ($request->file('file')->isValid()) {
          $extension = $request->file->extension();
          $name = $sale->id . '-' . $sale->created_at->format('d-m-Y-H-m-s');
          $sale->file = $request->file('file')->storeAs('sales', $name . "." . $extension);
          $sale->save();
        }
      }

      DB::commit();
      return response()->json(['message' => 'Pago Registrado '], 200);
    } catch (Exception $e) {
      DB::rollBack();
      return response()->json(['error' => $e], 400);
    }
  }

  public function bankPdf($id)
  {
    $sale = Sale::with(['client:id,name,last_name,dni,phone,address,email', 'bank'])->find($id);
    $pdf = PDF::loadView('pdf.sale-bank', ['sale' => $sale])->download('Factura-' . $sale->id . '.pdf');
    return $pdf;
  }

  public function wbankPdf($id)
  {
    $sale = Sale::with(['client:id,name,dni', 'bank'])->find($id);
    return view('pdf.sale-bank', compact('sale'));
  }

  public function indexAgent($id)
  {

    $sales = new SaleCollection(Sale::where('agent_id', $id)->with(['agent:id,username', 'client:id,name,last_name', 'bank:id,name'])->get()->reverse());

    return response()->json(['sales' => $sales]);
  }

  public function show($id)
  {
    $sale = Sale::find($id);
    return response()->json($sale);
  }

  public function edit($id)
  {
    $sale = Sale::with('client:id,name,dni,phone,accumulated')->find($id);
    return response()->json($sale);
  }

  public function update(Request $request, $id)
  {

    $sale = Sale::findOrFail($id);
    $client = Client::where('id', $sale->client_id)->first();
    if ($sale->iso == 'EUR') {
      if ($sale->active == 1 || $sale->active == 3) {
        if ($request->iso == 'EUR') {
          if ($request->client_id != $sale->client_id) {
            $client->accumulated = $client->accumulated - $sale->amount;
            $client->save();

            $client = Client::where('id', $request->client_id)->first();
            $client->accumulated = $client->accumulated + $request->amount;
            $client->save();
          } else {
            $client->accumulated = ($client->accumulated - $sale->amount) + $request->amount;
            $client->save();
          }
        } else {
          $client->accumulated = $client->accumulated - $sale->amount;
          $client->save();
        }
      }
    }


    $sale->update([
      'agent_id' => $request->agent_id,
      'bank_id' => $request->bank_id,
      'client_id' => $request->client_id,
      'description' => $request->description,
      'total_btc' => $request->total_btc,
      'iso' => $request->iso,
      'operation_method' => $request->operation_method,
      'amount' => $request->amount,
      'agency_num' => $request->agency_num,
      'code' => $request->code,
      'user_id' => Auth::user()->id,
    ]);

    return response()->json(['message' => 'Pago Actualizado']);
  }

  public function desactive(Request $request)
  {
    $sale = Sale::find($request->id);
    $sale->active = 0;
    $sale->save();

    $balance = Balance::active()->first();
    if ($balance) {
      $balance->amount = $balance->amount + $sale->total_btc;
      $balance->save();
    }
    if ($sale->iso == 'EUR') {
      $client = Client::find($sale->client_id);
      $client->accumulated = $client->accumulated - $sale->amount;
      $client->save();
    }

    return response()->json(['message' => 'Desaprobado']);
  }

  public function activate(Request $request, $id)
  {
    $sale = Sale::find($request->id);

    $balance = Balance::active()->first();

    if ($balance && $sale->active != 1 && $sale->active != 3) {
      $balance->amount = $balance->amount - $sale->total_btc;
      $balance->save();
    }

    if ($sale->iso == 'EUR' && $sale->active != 1 && $sale->active != 3) {
      $client = Client::find($sale->client_id);
      $client->accumulated = $client->accumulated + $sale->amount;
      $client->save();
    }
    $sale->active = $request->active;
    $sale->save();

    return response()->json(['message' => 'Cobro Aprobado']);
  }

  public function destroy($id)
  {
    $sale = Sale::findOrFail($id);

    if ($sale->file) {
      $delete[] = Storage::delete($sale->file);
    }

    $sale->delete();
    return response()->json(['message' => 'La venta ha sido Eliminada']);
  }

  public function imgDownload($id)
  {
    $sale = Sale::findOrFail($id);
    return Storage::download($sale->file);
  }
}
