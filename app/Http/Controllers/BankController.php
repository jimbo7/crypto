<?php

namespace App\Http\Controllers;

use DB;
use App\Bank;
use App\Http\Traits\HelpersTrait;
use Illuminate\Http\Request;

class BankController extends Controller
{
    use HelpersTrait;

    public function __construct()
    {
        $this->middleware('admin', ['except' => ['indexActive', 'index']]);
    }

    public function index()
    {
        $banks = Bank::orderBy('id', 'desc')->get();
        return response()->json(['banks'=>$banks]);
    }

    public function indexActive()
    {
        $banks = Bank::orderBy('id', 'desc')->active()->get();
        return response()->json(['banks'=>$banks]);
    }

    public function store(Request $request)
    {
        $bank = new Bank([
            'name' => strtoupper($this->deleteAccent($request->name)),
            'account_code' => strtoupper($this->deleteAccent($request->account_code)),
            'account_holder' => strtoupper($this->deleteAccent($request->account_holder)),
            'description' => $request->description,
            'email' => $request->email,
            'dni_holder' => $request->dni_holder,
            'phone_holder' => $request->phone_holder,
            'address' => $request->address,
            'office_num' => $request->office_num,
            'bci' => $request->bci,
            'active' => 1,
        ]);
        $bank->save();

        return response()->json(['message' =>'Cuenta Guardada']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $bank
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bank = Bank::find($id);
        return response()->json($bank);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = Bank::find($id);
        return response()->json($bank);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
      try {
        DB::beginTransaction();
        $bank = Bank::find($id);
        $bank->update([
          'name' => strtoupper($this->deleteAccent($request->name)),
          'account_code' => strtoupper($this->deleteAccent($request->account_code)),
          'account_holder' => strtoupper($this->deleteAccent($request->account_holder)),
          'description' => $request->description,
          'email' => $request->email,
          'dni_holder' => $request->dni_holder,
          'phone_holder' => $request->phone_holder,
          'address' => $request->address,
          'office_num' => $request->office_num,
          'bci' => $request->bci,
        ]);
        DB::commit();
          return response()->json(['message' =>'actualizado']);
      } catch (Exception $e) {
          DB::rollBack();
          return response()->json(['error'=>$e], 400);
      }
    }

    public function desactive(Request $request)
    {
        $bank = Bank::find($request->id);
        $bank->active = 0;
        $bank->save();

        return response()->json(['message'=>'desactivado']);
    }

    public function activate(Request $request)
    {
        $bank = Bank::find($request->id);
        $bank->active = 1;
        $bank->save();

        return response()->json(['message' =>'activado']);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = Bank::findOrFail($id);
        $bank->delete();

        return response()->json(['message'=>'Cuenta Eliminada']);
    }


}
