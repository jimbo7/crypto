<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = Currency::active()->get();
        return response()->json(['currencies' => $currencies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|min:3',
            'iso' => 'required|string|min:2',
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(), 403);
        }

        $currency = new Currency ([
            'name' => $request->name,
            'iso' => $request->iso,
            'symbol' => $request->symbol,
            'active' => $request->active
        ]);

        $currency->save();

        return response()->json(['message' => 'Saved'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function show(Currency $currency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function edit(Currency $currency)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Currency $currency)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|min:3',
            'iso' => 'required|string|min:2',
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(), 403);
        }

        $currency->update([
            'name' => $request->name,
            'iso' => $request->iso,
            'symbol' => $request->symbol,
            'active' => $request->active
        ]);

        $currency->save();

        return response()->json(['message' => 'Saved'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        //
    }
}
