<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
        return response()->json(
            [
                'status' => 'success',
                'users' => $users->toArray()
            ], 200);
    }

    public function show(Request $request, $id)
    {
        $user = User::find($id);

        return response()->json(
	        [
	            'status' => 'success',
	            'user' => $user->toArray()
	        ], 200);
    }

    public function register(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try {
            DB::beginTransaction();
            $user = new User();
            $user->name = ucwords(mb_strtolower($request->name));
            $user->phone = $request->phone;
            $user->email = strtolower($request->email);
            $user->username = $request->username;
            $user->wallet_num = $request->wallet_num;
            $user->password = bcrypt($request->password);
            $user->active = 1;
            $user->save();

            DB::commit();
            return response()->json(['message'=> 'Registrado '], 200);
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(['error'=>$e], 400);;
            }
    }
}
