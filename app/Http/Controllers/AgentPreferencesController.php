<?php

namespace App\Http\Controllers;

use DB;
use App\Agent;
use App\AgentPreferencesClient as Agpc;
use App\Http\Traits\HelpersTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AgentPreferencesController extends Controller
{
    use HelpersTrait;

    public function get()
    {
        $id = Auth::user()->agent_id;
        $preferences = Agpc::where('agent_id', $id)->get();
        return response()->json(['preferences' => $preferences]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $id = Auth::user()->id;
        if ($id != $request->agent_id) {
          return response()->json(['error'=> 'No autorizado '], 405);

        }

        try {
            DB::beginTransaction();

            $preferences = new Agpc([
              'agent_id' => $id,
              'agent_name' => $request->agent_name,
              'agent_user' => $request->agent_user,
              'agent_web' => $request->agent_web,
              'header_description' => $request->header_description,
              'sb_left' => $request->sb_left,
              'instagram' => $request->instagram,
              'facebook' => $request->facebook,
              'twitter' => $request->twitter,
              'sb_right' => $request->sb_right

            ]);
            $preferences->save();

            if ($request->hasFile('body_image')) {

              $image = $request->file('body_image');
              $url = $this->saveImage($image,'agents/'.$id,$preferences->id);

              $preferences->body_image = $url;

              $preferences->save();
            }

            DB::commit();
            return response()->json(['message'=> 'Pago Registrado '], 200);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error'=>$e], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agpc  $agpc
     * @return \Illuminate\Http\Response
     */
    public function show(Agpc $agpc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agpc  $agpc
     * @return \Illuminate\Http\Response
     */
    public function edit(Agpc $agpc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agpc  $agpc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agpc $agpc)
    {
      $id = Auth::user()->id;
      if ($id != $request->agent_id && $agpc->agent_id) {
        return response()->json(['error'=> 'No autorizado '], 405);

      }

      try {
          DB::beginTransaction();

          $agpc->update([
            'agent_name' => $request->agent_name,
            'agent_user' => $request->agent_user,
            'agent_web' => $request->agent_web,
            'header_description' => $request->header_description,
            'sb_left' => $request->sb_left,
            'instagram' => $request->instagram,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'sb_right' => $request->sb_right

          ]);

          if ($request->hasFile('body_image')) {

            $image = $request->file('body_image');
            $url = $this->saveImage($image,'agents/'.$id,$preferences->id);

            $agpc->update([
              'body_image' => $url,
            ]);

          }

          DB::commit();
          return response()->json(['message'=> 'Preferencias Actualizadas '], 200);

      } catch (Exception $e) {
          DB::rollBack();
          return response()->json(['error'=>$e], 400);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agpc  $agpc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agpc $agpc)
    {
        //
    }
}
