<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
  public function index()
  {
      $countries = Country::orderBy('id', 'asc')->get();
      return response()->json(['countries'=>$countries]);
  }

  public function store(Request $request)
    {
        $country = new Country([
            'name' => strtoupper($request->name),
            'iso' => strtoupper($request->iso),
            'active' => 1
        ]);
        $country->save();

        return response()->json(['message' =>$country->name.' Guardado']);
    }

    public function update(Request $request, Country $country)
    {
        $country->update([
            'name' => strtoupper($request->name),
            'iso' => strtoupper($request->iso),
            'active' => 1
        ]);

        return response()->json(['message' =>$country->name.' Actualizado']);
    }

    public function desactive(Country $country)
    {
        $country->active = 0;
        $country->save();

        return response()->json(['message'=> $country->name. ' Desactivado']);
    }

    public function activate(Country $country)
    {
        $bank->active = 1;
        $bank->save();

        return response()->json(['message'=> $country->name. ' activado']);
    }

    public function destroy(Country $country)
    {
        $country->delete();

        return response()->json(['message'=>'Pais Eliminado']);
    }
}
