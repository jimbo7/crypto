<?php

namespace Resources;

use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientPayment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => ($this->last_name) ? ucfirst($this->name. ' '. $this->last_name): ucfirst($this->name),
            'last_name' =>($this->last_name) ? ucfirst($this->last_name) : '',
            'full_name' =>($this->last_name) ? ucfirst($this->name. ' '. $this->last_name): ucfirst($this->name),
            'phone' => $this->phone,
            'accumulated' => $this->accumulated,
            'dni' =>($this->type_id) ?$this->type_id. '-' .$this->dni: $this->dni,
        ];
    }
}
