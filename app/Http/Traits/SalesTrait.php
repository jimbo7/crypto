<?php

namespace App\Http\Traits;

use DB;
use Image;
use Auth;
use App\Sale;
use Carbon\Carbon;
use App\Http\Requests\StoreSale;
use App\Http\Traits\HelpersTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

trait SalesTrait
{
    use HelpersTrait;

    public function saleTraitStore($request)
    {

        try {

            $profits = ($request->amount / $request->gross_btc) - $request->total_btc;

            $sale = new Sale([
              'user_id' => Auth::user()->id,
              'agent_id' => (Auth::user()->agent_id) ? Auth::user()->agent_id : '',
              'bank_id' => $request->bank_id,
              'client_id' => $request->client_id,
              'description' => $request->description,
              'price_btc' => $request->price_btc,
              'total_btc' => $request->total_btc,
              'gross_btc' => $request->gross_btc,
              'profits_btc' => $profits,
              'iso' => $request->iso,
              'operation_method' => $request->operation_method,
              'amount' => $request->amount,
              'agency_num' => $request->agency_num,
              'code' => $request->code,
              'user_ip' => '',
              'active' => $request->active,
            ]);
            $sale->save();

            if ($request->hasFile('file')) {

              if ($request->file('file')->isValid()) {
                $extension = $request->file->extension();
                $name = $sale->id. '-'. $sale->created_at->format('d-m-Y-H-m-s');
                $url = $request->file('file')->storeAs('sales', $name .".".$extension);
                $sale = Sale::where('id', $sale->id)->update(['file' => $url]);
              }
            }

            DB::commit();
            return response()->json(['message'=> 'Pago Registrado '], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error'=>$e], 400);
        }
    }
}
