<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'HomeController@index')->name('index');
Route::get('/contactanos/', 'HomeController@contact')->name('contact');
Route::get('/acerca-de-nosotros/', 'HomeController@about')->name('about');
Route::get('/nuestros-servicios/', 'HomeController@services')->name('services');
Route::get('/rate', 'PriceController@ratePage')->name('rate');

// pdf of sales
Route::get('/sale/bank-pdf/{id}', 'SaleController@bankPdf');
Route::get('/sale/wbank-pdf/{id}', 'SaleController@wbankPdf');

// LEGAL
Route::get('/legal/politicas-de-privacidad', 'HomeController@policy')->name('policy');
Route::get('/legal/terminos-y-condiciones/', 'HomeController@terms')->name('terms');
Route::get('/legal/aml-kyc/', 'HomeController@amlKyc')->name('aml-kyc');

// VIEW TO RELOAD PRICES FROM LOCALBITCOIN
Route::get('/prices', function () {
    return view('welcome');
});

// CLIENT REGISTRATION & EMAIL - PHONE VERIFICATION
Route::get('/{username}/registro-de-datos', 'HomeController@clientRegister');
Route::post('/client-store', 'ClientController@storeFromWeb')->name('client-store');

Route::get('/clients-verify-sms/{client}', 'HomeController@clientVerifySms')->name('clients-verify-sms');

Route::post('/clients-check-sms/{client}', 'ClientController@clientCheckSms')->name('clients-check-sms');


// REGISTRO Y VERIFICACION DE AGENTE AL REGISTRARSE
Route::get('agent-clients/{id}', 'AgentController@agentClients');

Route::post('agents/register', 'AgentController@agentRegister');
Route::post('agents/email-verify', 'AgentController@email');
Route::post('agents/user-verify', 'AgentController@user');

// SPA ROUTES
Route::get('/access/{any?}', function (){
    return view('admin.admin');
})->where('any', '.*');
Route::get('/admin/{any?}', function (){
    return view('admin.admin');
})->where('any', '.*');
Route::get('/agents/{any?}', function (){
    return view('admin.admin');
})->where('any', '.*');

// OLD RATE ROUTE
Route::get('tasa', 'PriceController@exchange');

Route::get('client-total', 'ClientController@totalCalculate');

// CUSTOMER ACCUMULATED RESET
Route::get('client-reset', 'ClientController@resetMounthly');

Route::get('reset', 'PriceController@reset');

// ARTISAN CLEAR ROUTES
Route::get('/cacheter', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});
