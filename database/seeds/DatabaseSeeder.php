<?php

use App\User;
use App\Client;
use App\Sale;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call('CountrySeeder');
      // $this->call('ClientSeeder');
        // User::create([
        //     'name' => 'Admin',
        //     'email' => 'admin@test.com',
        //     'password' => Hash::make('admin'),
        //     'role' => 2
        // ]);
        // User::create([
        //     'name' => 'User',
        //     'email' => 'user@test.com',
        //     'password' => Hash::make('secret'),
        //     'role' => 1
        // ]);

        // $faker = Faker::create();
        // for ($i=0; $i < 50; $i++) {
        //     \DB::table('sales')->insert([
        //         'bank_id' => 1,
        //         'client_id'=> $faker->numberBetween(41,61),
        //         'amount' => $faker->randomNumber(2),
        //         'operation_method' => 'transf',
        //         'active'=>1,
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now()
        //       ]);
        // }
    }
}
