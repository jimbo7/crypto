<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
		for ($i=0; $i < 50; $i++) {
		    \DB::table('clients')->insert(array(
		           'name' => $faker->name,
		           'dni' => Str::random(10),
		           'agent_id'=> 1,
		           'active'=>1,
		           'created_at' => date('Y-m-d H:m:s'),
		           'updated_at' => date('Y-m-d H:m:s')
		    ));
		}
    }
}
