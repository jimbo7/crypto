<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinusToBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balances', function (Blueprint $table) {
          $table->decimal('amount_minus_operation', 10,8)->nullable();
          $table->decimal('amount_sum_operation', 10,8)->nullable();
          $table->string('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balances', function (Blueprint $table) {
          $table->dropColumn(['amount_minus_operation','amount_sum_operation','notes']);
        });
    }
}
