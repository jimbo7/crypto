<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->boolean('approved')->default(0)->nullable()->comment('aproved "1" or waiting aprovation "0"');
            $table->string('email', 50)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('type_id', 10)->nullable();
            $table->string('id_f', 150)->nullable()->comment('url dni_front');
            $table->string('id_b', 150)->nullable()->comment('url dni_back');
            $table->date('birthdate', 20)->nullable();
            $table->decimal('accumulated', 15,2)->nullable()->comment('amount accumulated in month');
            $table->string('term_selfie', 150)->nullable()->comment('url photo term selfie with dni');
            $table->string('auth_image', 150)->nullable()->comment('url photo document to accept the terms.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn(['approved', 'email', 'phone','type_id','id_f','id_b','birthdate','accumulated','term_selfie','auth_image']);
        });
    }
}
