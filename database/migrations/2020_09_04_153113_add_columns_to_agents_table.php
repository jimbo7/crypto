<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->string('type_id', 10)->nullable();
            $table->string('dni', 30)->unique()->nullable();
            $table->text('id_f')->nullable()->comment('url dni front photo');
            $table->text('id_b')->nullable()->comment('url dni back photo');
            $table->date('birthdate', 20)->nullable();
            $table->text('term_selfie')->nullable()->comment('url photo term selfie with dni');
            $table->text('auth_image')->nullable()->comment('url photo document to accept the terms.');
            $table->decimal('discount', 5,3)->nullable()->default(0.000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->dropColumn(['type_id', 'dni', 'id_f', 'id_b', 'birthdate', 'term_selfie', 'auth_image', 'discount']);
        });
    }
}
