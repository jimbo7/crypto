<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVerifyToClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('email_confirm_token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('sms_confirm_token')->nullable();
            $table->timestamp('sms_verified_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
          $table->dropColumn(['email_confirm_token', 'email_verified_at', 'sms_confirm_token', 'sms_verified_at']);
        });
    }
}
