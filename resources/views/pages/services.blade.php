@extends('layouts.app-page')

@section('content')
    <!-- start slider section -->
    <!-- wallet -->
    <div class="wallet">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="titlepage text_align_center">
                        <h2>Nuestros Servicios</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div id="wa_hover" class="wallet_box text_align_center">
                        <i><img src="images/wa1.svg" alt="#"/></i>
                        <h3>Intercambios en LocalBitcoints.com </h3>
                        <p>Trabajamos con wallets internas de Localbitcoins.com para que los intercambios sean instantáneos. </p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div id="wa_hover" class="wallet_box text_align_center">
                        <i><img src="images/wa2.svg" alt="#"/></i>
                        <h3>Privacidad de su Información</h3>
                        <p>Registro de todas las transacciones, en servidor privado para tu seguridad.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div id="wa_hover" class="wallet_box text_align_center">
                        <i><img src="images/wa3.svg" alt="#"/></i>
                        <h3>APP MÓVIL</h3>
                        <p>Aplicacion móvil en contrucción.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div id="wa_hover" class="wallet_box text_align_center">
                        <i><img src="images/wa4.svg" alt="#"/></i>
                        <h3>Soporte</h3>
                        <p>Ayuda y soporte a nuestros clientes en temas financieros, legales, y criptográficos.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end wallet -->
@endsection
