@extends('layouts.app-page')

@section('content')
    <!-- about -->
    <div id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about_border">
                        <div class="row">
                            <div class="col-md-6 mt-5">
                                <div class="titlepage text_align_left" id="main">

                                </div>
                                <div class="about_text" >
                                    <h3>La Tasa de Cambio puede cambiar minuto a minuto</h3>
                                    @if ($pr)
                                        @foreach ($pr as $item)
                                            {{-- {{dd($item)}} --}}
                                            <h2> BTC - {{$item['iso']}} = {{$item['amount']}} {{ $item['symbol']}} </h2>
                                        @endforeach
                                    @endif
                                    {{-- <a class="read_more"  href=""></a> --}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="about_img">
                                    <figure><img class="img_responsive" src="images/about2.png" alt="#" /></figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end about -->
@endsection
