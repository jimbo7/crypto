@extends('layouts.app-page')

@section('content')
    <!-- terms -->
    <div class="wallet">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12" style="min-height: 80vh;">
                    <iframe width="100%" height="100%" src="{{asset('files/protect.pdf')}}" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- end terms -->
@endsection
