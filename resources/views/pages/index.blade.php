@extends('layouts.app-page')

@section('content')
    <!-- start slider section -->
    <div id="top_section" class=" banner_main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="container-fluid">
                                    <div class="carousel-caption relative">
                                        <div class="row d_flex">
                                            <div class="col-md-6">
                                                <div class="con_img">
                                                    <figure><img class="img_responsive" src="./images/top_img.png" alt="#" /></figure>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="bluid">
                                                    <h1>Compra BitCoin<br> con toda la confianza </h1>
                                                    <p>y seguridad que nuestra experiencia financiera-criptográfica nos avala.
                                                    </p>
                                                {{-- <a class="read_more" href="{{ url('/access/registrar')}}">Regístrate </a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container-fluid">
                                    <div class="carousel-caption relative">
                                        <div class="row d_flex">
                                            <div class="col-md-6">
                                                <div class="con_img">
                                                    <figure><img class="img_responsive" src="./images/top_img.png" alt="#" /></figure>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="bluid">
                                                    <h1>Expandiéndonos. <br> Estamos presentes en: </h1>
                                                    <p>España, Ecuador, Colombia y Reino Unido.
                                                    </p>
                                                    {{-- <a class="read_more" href="{{ url('/access/registrar')}}">Regístrate</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container-fluid">
                                    <div class="carousel-caption relative">
                                        <div class="row d_flex">
                                            <div class="col-md-6">
                                                <div class="con_img">
                                                    <figure><img class="img_responsive" src="./img/logo.png" alt="#" /></figure>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="bluid">
                                                    <h1>Crecemos constantemente <br> Tenemos ideas innovadoras de negocios</h1>
                                                    <p>y un proyecto ambioso en marcha.
                                                    </p>
                                                    {{-- <a class="read_more" href="{{ url('/access/registrar')}}">Regístrate </a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <span class="sr-only">Anterior</span>
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span class="sr-only">Siguiente</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end slider section -->
    <!-- wallet -->
    <div class="wallet">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div id="wa_hover" class="wallet_box text_align_center">
                        <i class="fa fa-btc fa-4x" aria-hidden="true"></i>
                        <h3>Cartera Local</h3>
                        <p>Trabajamos con wallets internas de LocalBitCoins.com para que los intercambios sean instantáneos.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div id="wa_hover" class="wallet_box text_align_center">
                        <i class="fa fa-user-secret fa-4x" aria-hidden="true"></i>
                        <h3>Mantenemos su información Privada</h3>
                        <p>Registro de todas las transacciones, en servidor privado para tu seguridad.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div id="wa_hover" class="wallet_box text_align_center">
                        <i class="fa fa-mobile fa-4x" aria-hidden="true"></i>
                        <h3>Pronto Aplicación Móvil</h3>
                        <p>Pronto lanzaremos nuestra app para móviles.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div id="wa_hover" class="wallet_box text_align_center">
                        <i class="fa fa-check-square-o fa-4x" aria-hidden="true"></i>
                        <h3>Satisfacción Garantizada</h3>
                        <p>Ayuda y soporte a nuestros clientes en temas financieros, legales, y criptográficos.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end wallet -->
    <!-- about -->
    <div id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about_border">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="titlepage text_align_left">
                                    <h2>Acerca de Nosotros</h2>
                                </div>
                                <div class="about_text">
                                    <p>En Cointrato, somos un equipo entusiasta, que queremos ayudarte a crecer; Seas parte de nuestros agentes, o bien si eres cliente.</p><p>Venimos operando en intercambios FIAT/BTC - BTC/FIAT, hace un año ya; Lo hemos venido haciendo con mucha pasión, y con todo cariño para brindarte el mejor servicio posible; pese a las dificultades que conyeva las nuevas ideas y tecnologias, pero estamos seguros y nuestra convicción es que la lucha se la gana luchando, y es lo que hacemos día a día en Cointrato, para servirte.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="about_img">
                                    <figure><img class="img_responsive" src="./images/about2.png" alt="#" /></figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end about -->
    <!-- graf -->
    <div class="graf">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs md-tabs graf_tab" id="myTabMD" role="tablist">
                        <li>
                            <a class="nav-link active" id="home-tab-md" data-toggle="tab" href="#tab1" role="tab" aria-controls="home-md" aria-selected="true">BitCoin(BTC)</a>
                        </li>
                        <li>
                            <a class="nav-link" id="profile-tab-md" data-toggle="tab" href="#tab2" role="tab" aria-controls="profile-md" aria-selected="false">BitCoin Cash(BCH)</a>
                        </li>
                        <li>
                            <a class="nav-link" id="contact-tab-md" data-toggle="tab" href="#tab3" role="tab" aria-controls="contact-md" aria-selected="false">BitCoin Lite(LTC)</a>
                        </li>
                    </ul>
                    {{-- <div class="tab-content card  graf_content" id="myTabContentMD">
                        <div class="tab-pane fade show active padi" id="tab1" role="tabpanel" aria-labelledby="home-tab-md">
                            <div class="row">
                                <div class="col-md-2 col-sm-4">
                                    <div class="usd text_align_center">
                                        <h4><span>USD</span> 123456.09 <br> Last Price </h4>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Daily Change </h4>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour Low </h4>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour High </h4>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Today Open </h4>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <div class="usd text_align_center">
                                        <h4>BTC 09876 <br> 24 hour volume </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="graf_bootom">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <figure>
                                                <h3 class="h3tota">Total Growth</h3>
                                                <img class="img_responsive" src="./images/graf1.jpg" alt="#" />
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <h3>LiveTrades <span style="color: #face34;" class="pa_ri">USD 123456.09</span></h3>
                                            <figure><img class="img_responsive" src="./images/graf2.jpg" alt="#" /></figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade padi" id="tab2" role="tabpanel" aria-labelledby="profile-tab-md">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4><span>USD</span> 123456.09 <br> Last Price </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Daily Change </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour Low </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour High </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Today Open </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>BTC 09876 <br> 24 hour volume </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="graf_bootom">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <figure>
                                                <h3 class="h3tota">Total Growth</h3>
                                                <img class="img_responsive" src="./images/graf1.jpg" alt="#" />
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <h3>LiveTrades <span style="color: #face34;" class="pa_ri">USD 123456.09</span></h3>
                                            <figure><img class="img_responsive" src="./images/graf2.jpg" alt="#" /></figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade padi" id="tab3" role="tabpanel" aria-labelledby="contact-tab-md">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4><span>USD</span> 123456.09 <br> Last Price </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Daily Change </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour Low </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour High </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Today Open </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>BTC 09876 <br> 24 hour volume </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="graf_bootom">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <figure>
                                                <h3 class="h3tota">Total Growth</h3>
                                                <img class="img_responsive" src="./images/graf1.jpg" alt="#" />
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <h3>LiveTrades <span style="color: #face34;" class="pa_ri">USD 123456.09</span></h3>
                                            <figure><img class="img_responsive" src="./images/graf2.jpg" alt="#" /></figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade padi" id="tab4" role="tabpanel" aria-labelledby="contact-tab-md">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4><span>USD</span> 123456.09 <br> Last Price </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Daily Change </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour Low </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour High </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Today Open </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>BTC 09876 <br> 24 hour volume </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="graf_bootom">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <figure>
                                                <h3 class="h3tota">Total Growth</h3>
                                                <img class="img_responsive" src="./images/graf1.jpg" alt="#" />
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <h3>LiveTrades <span style="color: #face34;" class="pa_ri">USD 123456.09</span></h3>
                                            <figure><img class="img_responsive" src="./images/graf2.jpg" alt="#" /></figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade padi" id="tab5" role="tabpanel" aria-labelledby="contact-tab-md">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4><span>USD</span> 123456.09 <br> Last Price </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Daily Change </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour Low </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> 24 Hour High </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>USD 123456.09 <br> Today Open </h4>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="usd text_align_center">
                                        <h4>BTC 09876 <br> 24 hour volume </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="graf_bootom">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <figure>
                                                <h3 class="h3tota">Total Growth</h3>
                                                <img class="img_responsive" src="./images/graf1.jpg" alt="#" />
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="growth text_align_center">
                                            <h3>LiveTrades <span style="color: #face34;" class="pa_ri">USD 123456.09</span></h3>
                                            <figure><img class="img_responsive" src="./images/graf2.jpg" alt="#" /></figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- end graf -->
    <!-- testimonial -->
    {{-- <div class="testimonial">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="border_testi">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="titlepage text_align_center">
                                    <h2>Comentarios de nuestros clientes</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row d_flex">
                            <div class="col-md-10 offset-md-1">
                                <div id="testimo" class="carousel slide our_testimonial" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#testimo" data-slide-to="0" class="active"></li>
                                        <li data-target="#testimo" data-slide-to="1"></li>
                                        <li data-target="#testimo" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <div class="container">
                                                <div class="carousel-caption posi_in">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="testomoniam_text text_align_center">
                                                                <i><img src="./images/clint.jpg" alt="#"/></i>
                                                                <h3>MorGan Den</h3>
                                                                <span>BitCoin</span>
                                                                <img src="./images/icon.png" alt="#" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="testomoniam_text text_align_left">
                                                                <p>more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and webmore-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and webmore-or-less normal distribution of</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <div class="container">
                                                <div class="carousel-caption posi_in">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="testomoniam_text text_align_center">
                                                                <i><img src="./images/clint.jpg" alt="#"/></i>
                                                                <h3>MorGan Den</h3>
                                                                <span>BitCoin</span>
                                                                <img src="./images/icon.png" alt="#" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="testomoniam_text text_align_left">
                                                                <p>more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and webmore-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and webmore-or-less normal distribution of</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <div class="container">
                                                <div class="carousel-caption posi_in">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="testomoniam_text text_align_center">
                                                                <i><img src="./images/clint.jpg" alt="#"/></i>
                                                                <h3>MorGan Den</h3>
                                                                <span>BitCoin</span>
                                                                <img src="./images/icon.png" alt="#" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="testomoniam_text text_align_left">
                                                                <p>more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and webmore-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and webmore-or-less normal distribution of</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#testimo" role="button" data-slide="prev">
                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#testimo" role="button" data-slide="next">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- end testimonial -->
    <!-- works -->
    {{-- <div class="works">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="titlepage text_align_center">
                        <h2>How It Works</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="works_vedio">
                        <figure><img class="img_responsive" src="./images/work.png" alt="#"></figure>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- works -->
    <!-- contact -->
    <div class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="titlepage text_align_center">
                        <h2>Comuníquese con nosotros</h2>
                    </div>
                </div>
                <div class=" col-md-10 offset-md-1">
                    <form id="request" class="main_form">
                        <div class="row">
                            <div class="col-md-6 ">
                                <input class="contactus" placeholder="Nombre Completo" type="text" name="Nombre Completo">
                            </div>
                            <div class="col-md-6">
                                <input class="contactus" placeholder="Email" type="email" name="Email">
                            </div>
                            <div class="col-md-6">
                                <input class="contactus" placeholder="Número de Teléfono" type="text" name="Número de Teléfono">
                            </div>
                            <div class="col-md-6">
                                <textarea class="textarea" placeholder="Escriba un mensaje" type="textarea" name="message"></textarea>
                            </div>
                            <div class="col-md-12">
                                <button class="send_btn">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end contact -->
@endsection
