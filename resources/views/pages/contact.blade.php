@extends('layouts.app-page')

@section('content')
    <!-- start slider section -->
    <div class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="titlepage text_align_center">
                        <h2>Comuníquese con nosotros</h2>
                    </div>
                </div>
                <div class=" col-md-10 offset-md-1">
                    <form id="request" class="main_form">
                        <div class="row">
                          <div class="col-md-6 ">
                            <input class="contactus" placeholder="Nombre Completo" type="text" name="Nombre Completo">
                            </div>
                            <div class="col-md-6">
                                <input class="contactus" placeholder="Email" type="email" name="Email">
                            </div>
                            <div class="col-md-6">
                                <input class="contactus" placeholder="Número de Teléfono" type="text" name="Número de Teléfono">
                            </div>
                            <div class="col-md-6">
                                <textarea class="textarea" placeholder="Escriba un mensaje" type="textarea" name="message"></textarea>
                            </div>
                            <div class="col-md-12">
                                <button class="send_btn">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end contact -->
@endsection
