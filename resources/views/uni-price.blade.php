<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Scripts -->
  <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
  <!-- Fonts -->
  <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <style>
      body{
        color: cornsilk;
        background-color: #323233;
        font-size: 30px !important;
      }
      h5 {
        font-size: 30px !important;
      }
      h3, h2 {
          font-size: 40px;
      }

      .title{
        background-color: #001ec1;
        padding: 15px;
        font-weight: bold;
        color: goldenrod;
        box-shadow: 0 8px 7px 4px rgba(0,0,0,0.2), 0 3px 10px 0 rgba(0,0,0,0.24), 0 5px 10px 0 rgba(0,0,0,0.3);
      }
      .main{
        background-color: #001ec1;
        padding: 15px;
        box-shadow: 0 8px 7px 4px rgba(0,0,0,0.2), 0 3px 10px 0 rgba(0,0,0,0.24), 0 5px 10px 0 rgba(0,0,0,0.3);
      }
  </style>
</head>
<body class="container m-5">
    <div class=" header center" id="app">
        <h3 class="title m-5">Cointrato.com</h3>
        <h5 class="m-5">La tasa de Cambio puede cambiar minuto a minuto</h5>
    </div>
    <div id="main" class="main m-5 center">

    </div>
    <span></span>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
       <script type = "text/javascript">

       jQuery(document).ready(function() {
        // your code here
            getPrice();
        });
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         function getPrice(){
            //we will send data and recive data fom our AjaxController
            //alert("im just clicked click me");
            $.ajax({
               url:'./api/price/get',
               type:'get',
               success:  function (response) {
                    var prices = response.prices;
                    var str = '<h3>Tasa al Momento, Precio en : </h3><h2> ' + prices['notes']  +' '+ prices['amount_format'] +' </h2>'

                    // console.log(str);
                    $('#main').html(str);
                    },
                    statusCode: {
                        404: function() {
                            alert('web not found');
                        }
                    },
                    error:function(x,xs,xt){
                        window.open(JSON.stringify(x));
                        //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
                    }
                });
            }
            setInterval(getPrice, 50000);
       </script>
</html>
