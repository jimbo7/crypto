<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}">
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <title>Registro de Datos</title>
    <meta name="keywords" content="btc, euros, EUR, compra btc, venta btc, remesas españa, remesas, enviar euro, enviar dinero">
    <meta name="description" content="Compra Venta BTC Euro Peso Colombiano">
    <meta name="author" content="jonay medina jonaymedinadev@gmail.com">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
        <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
      <script src="{{ asset('js/custom.js') }}"></script>
    <style>
      html {height: -webkit-fill-available;}

    </style>
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slicknav.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <!-- responsive-->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <!-- awesome fontfamily -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
      body{
        background-size:auto;
        padding-bottom: 50px;
      }
    </style>

</head>
<body class="main-layout">
  @yield('content')
      <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/jquery-3.0.0.min.js') }}"></script>

        <script src="{{ asset('js/jquery.slicknav.min.js') }}"></script>
        @yield('scripts')
      </body>
  @yield('scripts')
</body>
</html>
