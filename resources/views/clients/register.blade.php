@extends('layouts.app-client')

@section('content')
<div class="container">
  <div class="card bg-light mt-5">
    <article class="card-body m-auto container">
      <h4 class="card-title text-primary mt-3 text-center text-uppercase">Formulario de Registro de Agente Autorizado {{ $agent->username}}</h4>
      <h4 class="card-title text-primary mt-3 text-center">Por favor valida tu datos</h4>
      @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
      @endif

      <form class="form-row" method="post" enctype="multipart/form-data" action="{{ route('client-store') }}">
        @csrf
        <input type="hidden" name="agent_id" value="{{ $agent->id}}">
        <div class="col-md-6 col-sm-12 mb-3">
          <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-user"></i> </span>
            </div>
              <input id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Ingrese Nombres por favor" type="text" value="{{ old('name') }}" required autocomplete="name" autofocus>

              @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div> <!-- form-group// -->
        </div>
        <div class="col-md-6 col-sm-12 mb-3">
          <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-user"></i> </span>
            </div>
              <input id="las_name" name="last_name" class="form-control @error('las_name') is-invalid @enderror" placeholder="Ingrese Apellidos" type="text" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

              @error('Last_name')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div> <!-- form-group// -->
        </div>
        <div class="col-md-6 col-sm-12 mb-3">
          <span class="input-group-text"> <i class="fa fa-address-book"></i>DNI/NIF/NIE/PASSAPORTE</span>
          <div class="form-group input-group">
            <select name="type_id" id="type_id" class="form-control" required>
              <option value="" selected disabled hidden>Seleccione una Opcion</option>
              <option value="Dni">DNI</option>
              <option value="Nie">NIE</option>
              <option value="Nif">NIF</option>
              <option value="Pasaporte">PASAPORTE</option>
              <option value="Cedula">Id o Cedula Pais de Origen</option>
            </select>

            @error('type_id')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror

          </div> <!-- form-group// -->
        </div>
        <div class="col-md-6 col-sm-12 mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text"> <i class="fa fa-address-card"></i> </span>
          </div>
          <input  id="dni" name="dni" class="form-control @error('dni') is-invalid @enderror" placeholder="Número ID" type="text" value="{{ old('dni') }}" required autocomplete="dni" autofocus>

            @error('dni')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-md-6 col-sm-12">
          <span class="input-group-text"> Fecha de vencimiento de Dni/Pasaporte/Nif</span>
          <div class="form-group input-group">
            <input id="expiration_date_dni" name="expiration_date_dni" class="form-control @error('expiration_date_dni') is-invalid @enderror" placeholder="Cumpleaños" type="date" value="{{ old('expiration_date_dni') }}" required autocomplete="expiration_date_dni" autofocus>

            @error('expiration_date_dni')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div> <!-- form-group// -->
        </div>
        <div class="col-md-6 col-sm-12">
          <span class="input-group-text form group"> Fecha de Nac. </span>
          <div class="form-group input-group">
            <input id="birthdate" name="birthdate" class="form-control @error('birthdate') is-invalid @enderror" placeholder="Cumpleaños" type="date" value="{{ old('birthdate') }}" required autocomplete="birthdate" autofocus>

            @error('birthdate')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div> <!-- form-group// -->
        </div>
        <div class="col-md-6 col-sm-12 mb-3">
          <label for="nation" class="input-group-text form_group">Donde te encuentras (Nacionalidad o pais de residencia):</label>
          <div class="for-group input-group">
            <select name="nation_id" id="nation" class="form-control" required value="{{ old('nation_id') }}" >
              <option value="" selected disabled hidden>Nacionalidad: </option>
                @foreach($countries as $nation)
                    <option value="{{$nation->id}}">{{ucfirst($nation->name)}}</option>
                @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 mb-3">
          <div class="form-row"></div>
          <span class="input-group-text">Ingrese numero sin el prefijo del pais (+34 / +58/)</span>
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-phone"></i></span>
            </div>
            <input type="text" readonly class="input-group-text col-md-2 col-sm-3" name="phone_code" id="phone_code"  value="{{ old('phone_code') }}"  autocomplete="phone_code">
            <input name="phone" class="form-control" placeholder="Numero de Telefono" type="tel" value="{{ old('phone') }}" required autocomplete="phone" autofocus min="6" max="10">

            
          </div> <!-- form-group// -->
          @error('phone')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="col-md-6 col-sm-12 mb-3">
          <label for="country_id" class="input-group-text form_group">Pais donde naciste:</label>
          <div class="for-group input-group">
            <select name="country_id" id="country" class="form-control" >
                @foreach($countries as $value)
                    <option value="{{$value->id}}">{{ucfirst($value->name)}}</option>
                @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 mb-3">
          <fieldset class="form-group">
            <div class="row">
              <legend class="col-form-label col-sm-3 pt-0">Genero</legend>
              <div class="col-sm-9">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="gender" id="male" value="male">
                  <label class="form-check-label" for="male">Hombre</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="gender" id="female" value="female">
                  <label class="form-check-label" for="female">Mujer</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="gender" id="dont-say" value="not" >
                  <label class="form-check-label" for="dont-say">Prefirere no decirlo</label>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="col-md-6 col-sm-12 mb-3">
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
            </div>
            <input id="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Su Correo Electrónico" type="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div> <!-- form-group// -->
        </div>
        <div class="col-12 mb-3">
          <label class="input-group-text form_group"> <i class="fa fa-address-card">Ingrese Dirección</i> </label>
          <div class="form-group input-group">
            <input id="address" name="address" class="form-control placeholder="Ingrese Direcccion completa" type="text" value="{{ old('address') }}" required autocomplete="address" autofocus>

          </div> <!-- form-group// -->
        </div>
        <div class="col-12"></div>
        <div class="col-md-6 col-sm-12 mb-3 my-3">
          <div class="custom-file">
            <label for="id_f" class="form-label id-f-label">Subir Id Frontal</label>
            <input class="form-control form-control-sm" id="id_f"  name="id_f" type="file" required/>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 mb-3 my-3">
          <div class="custom-file">
            <label for="id_b" class="form-label id-b-label">Subir Id Reverso</label>
            <input class="form-control form-control-sm" id="id_b" name="id_b" type="file" required/>
          </div>
        </div>
        <div class="offset-md-4 col-12 mt-3">
            <div class="form-check">
              <input class="form-check-input" name="terms_accepted" value="1" type="checkbox" id="terms">
            </div>
            <span class="ml-3 h6">Aceptar</span>
            <a class="form_group h5" href="{{ route('policy')}}" target="_blank">Política de Privacidad</a>
            <span class="mx-1 h6">y</span>
            <a class="form_group h5" href="{{ route('aml-kyc')}}" target="_blank">Política AML</a>
        </div>
        <div class="col-md-4 offset-md-4">
          @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          <div class="form-group mt-5 my-3">
              <button type="submit" class="btn btn-primary btn-block"> Guardar Datos  </button>
          </div>
        </div>
          <!-- form-group// -->
      </form>
    </article>
  </div> <!-- card.// -->
</div>
<script>
  $(document).ready(function($){
    var countries = <?php echo json_encode($countries); ?>;
    nation = '';
    // console.log(countries);
    $('#nation').on("change", function(){
        nation = $('select[name="nation_id"]').val();

        $.each(countries, function()
        {
          var con = this;
            if(con.id == nation)
            {
                coun = con.phone_code;
                $("#phone_code").val('+'+coun);
                $("#prefix_phone").text('+'+coun);
            }
        });
    });
    $('#phone').on('change',function(){
      $('#phone').val(function(i, text) {
          return text.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
      });
    });
    $('body').on("click", function(){
      nation = $('select[name="nation_id"]').val();

        $.each(countries, function()
        {
          var con = this;
            if(con.id == nation)
            {
                coun = con.phone_code;
                $("#phone_code").val('+'+coun);
                $("#prefix_phone").text('+'+coun);
            }
        });
    })


  });
</script>
@endsection


