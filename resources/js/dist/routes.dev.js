"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.routes = void 0;

var _Register = _interopRequireDefault(require("./components/Register"));

var _Login = _interopRequireDefault(require("./components/Login.vue"));

var _Home = _interopRequireDefault(require("./components/Home"));

var _AdminHome = _interopRequireDefault(require("./components/Admin-home"));

var _index = _interopRequireDefault(require("./components/sales/index"));

var _Create = _interopRequireDefault(require("./components/sales/Create"));

var _Edit = _interopRequireDefault(require("./components/sales/Edit"));

var _History = _interopRequireDefault(require("./components/sales/History"));

var _index2 = _interopRequireDefault(require("./components/pay/index"));

var _Create2 = _interopRequireDefault(require("./components/pay/Create"));

var _Edit2 = _interopRequireDefault(require("./components/pay/Edit"));

var _index3 = _interopRequireDefault(require("./components/banks/index"));

var _index4 = _interopRequireDefault(require("./components/agents/index"));

var _Profile = _interopRequireDefault(require("./components/agents/Profile"));

var _index5 = _interopRequireDefault(require("./components/clients/index"));

var _Create3 = _interopRequireDefault(require("./components/clients/Create"));

var _Sender = _interopRequireDefault(require("./components/clients/Sender"));

var _Index = _interopRequireDefault(require("./components/prices/Index"));

var _Percent = _interopRequireDefault(require("./components/prices/Percent"));

var _Balance = _interopRequireDefault(require("./components/prices/Balance"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var routes = [{
  path: '/access/login',
  name: 'login',
  component: _Login["default"],
  meta: {
    onlyLogout: true
  }
}, {
  path: '/access/registrar',
  name: 'register',
  component: _Register["default"],
  meta: {
    onlyLogout: true
  }
}, {
  path: '/home',
  name: 'home',
  component: _Home["default"]
}, {
  path: '/admin/',
  name: 'admin-home',
  component: _AdminHome["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/agents',
  name: 'agents',
  component: _index4["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/balance',
  name: 'balances',
  component: _Balance["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/clients',
  name: 'clients',
  component: _index5["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/registrar-cliente/',
  name: 'create-client-admin',
  component: _Create3["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/banks',
  name: 'banks',
  component: _index3["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/sales',
  name: 'sales',
  component: _index["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/sales/create',
  name: 'create-sale',
  component: _Create["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/sales/edit/:id',
  name: 'edit-sale',
  component: _Edit["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/sales/history',
  name: 'sales-history',
  component: _History["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/prices/',
  name: 'prices',
  component: _Index["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/admin/percent/',
  name: 'percent',
  component: _Percent["default"],
  meta: {
    requireAuth: true,
    admin: true,
    agent: false
  }
}, {
  path: '/agents/',
  name: 'agent-home',
  component: _Home["default"],
  meta: {
    requireAuth: true,
    admin: false,
    agent: true
  }
}, {
  path: '/agents/pagos/',
  name: 'pay',
  component: _index2["default"],
  meta: {
    requireAuth: true,
    admin: false,
    agent: true
  }
}, {
  path: '/agents/pagos/registrar',
  name: 'create-pay',
  component: _Create2["default"],
  meta: {
    requireAuth: true,
    admin: false,
    agent: true
  }
}, {
  path: '/agents/pagos/editar/:id',
  name: 'edit-pay',
  component: _Edit2["default"],
  meta: {
    requireAuth: true,
    admin: false,
    agent: true
  }
}, {
  path: '/agents/registrar-cliente/',
  name: 'create-client',
  component: _Create3["default"],
  meta: {
    requireAuth: true,
    admin: false,
    agent: true
  }
}, {
  path: '/agents/clientes/',
  name: 'clients-agent',
  component: _Sender["default"],
  meta: {
    requireAuth: true,
    admin: false,
    agent: true
  }
}, {
  path: '/agents/profile/',
  name: 'agent-profile',
  component: _Profile["default"],
  meta: {
    requireAuth: true,
    admin: false,
    agent: true
  }
}];
exports.routes = routes;