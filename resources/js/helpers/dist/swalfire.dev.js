"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  updateF: function updateF(data, item) {
    return new Promise(function (res, rej) {
      Swal.fire({
        title: data.title,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#1565c0',
        cancelButtonColor: '##0000008a',
        confirmButtonText: 'Aceptar!',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          item._method = 'put';
          axios.post(data.url, item).then(function (response) {
            res(response.data.message);
          })["catch"](function (err) {
            console.log(err);
            rej(err);
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {}
      });
    });
  },
  activeF: function activeF(data) {
    var _this = this;

    return new Promise(function (res, rej) {
      Swal.fire({
        title: data.title,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#1565c0',
        cancelButtonColor: '##0000008a',
        confirmButtonText: 'Aceptar!',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var me = _this;
          axios.post(data.url, {
            _method: 'put'
          }).then(function (response) {
            res(response.data.message);
          })["catch"](function (err) {
            console.log(err);
            rej(err);
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {}
      });
    });
  },
  activeS: function activeS(data) {
    var _this2 = this;

    return new Promise(function (res, rej) {
      Swal.fire({
        title: data.title,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#1565c0',
        cancelButtonColor: '##0000008a',
        confirmButtonText: 'Aceptar!',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var me = _this2;
          axios.post(data.url, {
            _method: 'put',
            active: data.active
          }).then(function (response) {
            res(response.data.message);
          })["catch"](function (err) {
            console.log(err);
            rej(err);
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {}
      });
    });
  },
  desactiveF: function desactiveF(data) {
    var _this3 = this;

    return new Promise(function (res, rej) {
      Swal.fire({
        title: data.title,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#b31d2b',
        cancelButtonColor: '##0000008a',
        confirmButtonText: 'Aceptar!',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var me = _this3;
          axios.post(data.url, {
            _method: 'put'
          }).then(function (response) {
            res(response.data.message);
          })["catch"](function (err) {
            console.log(err);
            rej(err);
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {}
      });
    });
  },
  deleteF: function deleteF(data) {
    var _this4 = this;

    return new Promise(function (res, rej) {
      Swal.fire({
        title: data.title,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#1565c0',
        cancelButtonColor: '##0000008a',
        confirmButtonText: 'Aceptar!',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var me = _this4;
          axios.post(data.url, {
            _method: 'delete'
          }).then(function (response) {
            res(response.data.message);
          })["catch"](function (err) {
            console.log(err);
            rej(err);
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {}
      });
    });
  }
};
exports["default"] = _default;